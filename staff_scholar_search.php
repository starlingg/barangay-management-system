<?php
	// require the database connection
	require 'classes/conn.php';
	if(isset($_POST['search_bspermit'])){
		$keyword = $_POST['keyword'];
?>
	<table class="table table-hover text-center table-bordered table-responsive">

    <thead class="alert-info">
        <tr>
            <th> Actions</th>
            <th> Resident ID </th>
            <th> Surname </th>
            <th> First Name </th>
            <th> Middle Name </th>
            <th> Age </th>
            <th> Birthday </th>
            <th> Mothersname </th>
            <th> Occupation </th>
            <th> Fathersname </th>
            <th> Occupation </th>
            <th> Address </th>
            <th> School </th>
            <th> Course & Year </th>
            <th> Number </th>
             
        </tr>
    </thead>

    <tbody>     
        <?php
            
            $stmnt = $conn->prepare("SELECT * FROM `tbl_scholarship` WHERE `lname` LIKE '%$keyword%' or  `mi` LIKE '%$keyword%' or  `age` LIKE '%$keyword%' ");
            $stmnt->execute();
            
            while($view = $stmnt->fetch()){
        ?>
            <tr>
                <td>    
                    <form action="" method="post">
                        <a class="btn btn-success" target="blank"  style="width: 90px; font-size: 17px; border-radius:30px; margin-bottom: 2px;" href="staff_scholarship_print.php?id_resident=<?= $view['id_resident'];?>">Generate</a> 
                        <input type="hidden" name="id_bspermitid" value="<?= $view['id_bspermitid'];?>">
                       
                    </form>
                </td>
                <td> <?= $view['id_resident'];?> </td> 
                <td> <?= $view['lname'];?> </td>
                <td> <?= $view['fname'];?> </td>
                <td> <?= $view['mi'];?> </td>
                <td> <?= $view['age'];?> </td>
                <td> <?= $view['birthday'];?> </td>
                <td> <?= $view['mothersname'];?> </td>
                <td> <?= $view['mothersoccupation'];?> </td>
                <td> <?= $view['fathersname'];?> </td>
                <td> <?= $view['fathersoccupation'];?> </td>
                <td> <?= $view['address'];?> </td>
                <td> <?= $view['school'];?> </td>
                <td> <?= $view['course_year'];?> </td>
                <td> <?= $view['contact_number'];?> </td> 
            </tr>
        <?php
        }
        ?>
    </tbody>

</table>
<?php		
	}else{
?>
<table class="table table-hover text-center table-bordered table-responsive">

    <thead class="alert-info">
        <tr>
            
            <th> Actions</th>
            <th> Resident ID </th>
            <th> Surname </th>
            <th> First Name </th>
            <th> Middle Name </th>
            <th> Age </th>
            <th> Birthday </th>
            <th> Mothersname </th>
            <th> Occupation </th>
            <th> Fathersname </th>
            <th> Occupation </th>
            <th> Address </th>
            <th> School </th>
            <th> Course & Year </th>
            <th> Number </th>
        </tr>
    </thead>

    <tbody>
        <?php 
         $stmnt = $conn->prepare("SELECT * FROM `tbl_scholarship`   ");
            $stmnt->execute();
            
            while($view = $stmnt->fetch()){
        ?>
                <tr>
                    <td>    
                        <form action="" method="post">
                            <a class="btn btn-success" target="blank"  style="width: 90px; font-size: 17px; border-radius:30px; margin-bottom: 2px;" href="scholarship_print.php?id_resident=<?= $view['id_resident'];?>">Generate</a> 
                            <input type="hidden" name="id_bspermitid" value="<?= $view['id_bspermitid'];?>">
                            
                        </form>
                    </td>
                 
                <td> <?= $view['id_resident'];?> </td> 
                <td> <?= $view['lname'];?> </td>
                <td> <?= $view['fname'];?> </td>
                <td> <?= $view['mi'];?> </td>
                <td> <?= $view['age'];?> </td>
                <td> <?= $view['birthday'];?> </td>
                <td> <?= $view['mothersname'];?> </td>
                <td> <?= $view['mothersoccupation'];?> </td>
                <td> <?= $view['fathersname'];?> </td>
                <td> <?= $view['fathersoccupation'];?> </td>
                <td> <?= $view['address'];?> </td>
                <td> <?= $view['school'];?> </td>
                <td> <?= $view['course_year'];?> </td>
                <td> <?= $view['contact_number'];?> </td> 
                </tr>
            <?php
                }
            ?>
        <?php
            
        ?>
    </tbody>
    
</table>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/js/bootstrap-modalmanager.min.js" integrity="sha512-/HL24m2nmyI2+ccX+dSHphAHqLw60Oj5sK8jf59VWtFWZi9vx7jzoxbZmcBeeTeCUc7z1mTs3LfyXGuBU32t+w==" crossorigin="anonymous"></script>
<!-- responsive tags for screen compatibility -->
<meta name="viewport" content="width=device-width, initial-scale=1 shrink-to-fit=no">
<!-- custom css --> 
<link href="../BarangaySystem/customcss/regiformstyle.css" rel="stylesheet" type="text/css">
<!-- bootstrap css --> 
<link href="./bootstrap//css/bootstrap.css" rel="stylesheet" type="text/css"> 
<!-- fontawesome icons -->
<script src="https://kit.fontawesome.com/67a9b7069e.js" crossorigin="anonymous"></script>
<script src="./bootstrap//js/bootstrap.bundle.js" type="text/javascript"> </script>

<?php
	}
$con = null;
?>