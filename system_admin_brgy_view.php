<?php
    
    error_reporting(E_ALL ^ E_WARNING);
    ini_set('display_errors',0);
    require('classes/resident.class.php');
    $userdetails = $bmis->get_userdata();
    $bmis->validate_admin();
    $brgy_id = $_GET['brgy_id'];
    $brgy = $residentbmis->get_brgy_info($brgy_id);
    $brgy_srvs = $residentbmis->get_brgy_srvs($brgy_id);
    $email = $residentbmis->get_brgy_email($brgy_id);
    $residentbmis->update_brgy_info();
?>

<?php 
    include('system_admin_sidebar.php');
?>
<?php
 if(isset($_SESSION['online']) && $_SESSION['online']) {
  
} else {
   
   
    echo "<script> alert('Please login...'); </script>";
    echo "<script>(location.href = 'login.php');</script> ";
}
?>
<style>
    .input-icons i {
        position: absolute;
    }
        
    .input-icons {
        width: 30%;
        margin-bottom: 10px;
        margin-left: 34%;
    }
        
    .icon {
        padding: 10px;
        min-width: 40px;
    }
    .form-control{
        text-align: center;
    }
</style>

<!-- Begin Page Content -->

<div class="container-fluid">

    <!-- Page Heading -->

    <div class="row"> 
        <div class="col text-center"> 
            <h1> Barangay <?php echo $brgy['barangay']; ?></h1>
        </div>
    </div>
    <br />
    <div class="row"> 
        <div class="col-md-2"> </div> 
        <div class="col-md-8"> 
            <div class="card">
                <div class="card-header bg-primary text-white"> Information</div>
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data"> 
                            <div class="row"> 
                                <div class="col">
                                    <div class="form-group">
                                        <label for="province">Province:</label>
                                        <input name="province" type="text" class="form-control" value="<?= $brgy['province']?>">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="city">City:</label>
                                        <input name="city" type="text" class="form-control" value="<?= $brgy['city']?>">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="barangay">Barangay:</label>
                                        <input name="barangay" type="text" class="form-control" value="<?= $brgy['barangay']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col">
                                    <div class="form-group">
                                        <label for="zipcode">Zip Code:</label>
                                        <input name="zipcode" type="text" class="form-control" value="<?= $brgy['zipcode']?>">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="brgy_capt">Brgy. Capt.:</label>
                                        <input name="brgy_capt" type="text" class="form-control" value="<?= $brgy['brgy_capt']?>">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="account">Account:</label>
                                        <input name="account" type="text" class="form-control" value="<?= $email['email']?>">
                                    </div>
                                </div>
                            </div>
                            <hr>
                          
                            <!-- <div class="row">
                                <div class="col">
                                    <label>Supporting Evidence Photo:</label>
                                    <div class="custom-file form-group">
                                        <input type="file" onchange="readURL(this);" value="<?= $view['blot_photo']?>" class="custom-file-input" id="customFile" name="blot_photo" required>
                                        <label class="custom-file-label" for="customFile">Choose File Photo</label>
                                        <div class="valid-feedback">Valid.</div>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <h4>SERVICES</h4>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <?php 
                                if(unserialize($brgy_srvs['brgy_services']) == '' ) {
                                    include 'default_services.php';
                                } else {
                                    include 'with_services.php';
                                }
                            ?>
                            <br><br>
                            <div class="modal-footer">
                                <input name="brgy_id" type="hidden" value="<?= $brgy['brgy_id']?>">
                                <a type="button" href="system_admin_settings.php" class="btn btn-danger" 
                                style="width: 135px;
                                border-radius: 30px;
                                font-size: 18px;
                                color:white;" >Cancel</a>
                                <button type="submit" name="update_brgy_info" 
                                style="width: 135px;
                                border-radius: 30px;
                                font-size: 18px;" class="btn btn-success">Save changes</button>
                            </div>   
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2"> </div>
    </div>
    
</div>
<!-- End of Main Content -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/js/bootstrap-modalmanager.min.js" integrity="sha512-/HL24m2nmyI2+ccX+dSHphAHqLw60Oj5sK8jf59VWtFWZi9vx7jzoxbZmcBeeTeCUc7z1mTs3LfyXGuBU32t+w==" crossorigin="anonymous"></script>
<!-- responsive tags for screen compatibility -->
<meta name="viewport" content="width=device-width, initial-scale=1 shrink-to-fit=no">
<!-- custom css --> 
<link href="../BarangaySystem/customcss/regiformstyle.css" rel="stylesheet" type="text/css">
<!-- bootstrap css --> 
<link href="./bootstrap//css/bootstrap.css" rel="stylesheet" type="text/css"> 
<!-- fontawesome icons -->
<script src="https://kit.fontawesome.com/67a9b7069e.js" crossorigin="anonymous"></script>
<script src="./bootstrap//js/bootstrap.bundle.js" type="text/javascript"> </script>

<?php 
    include('dashboard_sidebar_end.php');
?>
