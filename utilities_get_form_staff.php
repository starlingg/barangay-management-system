<?php
ini_set('display_errors',0);
require('classes/resident.class.php');
$userdetails = $residentbmis->get_userdata();
$id_resident = $_GET['id_resident'];
$resident = $residentbmis->get_single_utilities($id_resident);
  ?>
<!DOCTYPE html>
<html id="clearance">
<style>
    @media print {
        .noprint {
        visibility: hidden;
         }
    }
    @page { size: auto;  margin: 4mm; }
</style>

 <head>
    <meta charset="UTF-8">
    <title>Barangay Information System</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="./bootstrap//css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="./bootstrap//css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="./bootstrap//css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap//css/morris-0.4.3.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="./bootstrap//css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap//css/select2.css" rel="stylesheet" type="text/css" />
    <script src="./bootstrap//css/jquery-1.12.3.js" type="text/javascript"></script>  
    
</head>
 <body class="skin-black" >
     <!-- header logo: style can be found in header.less -->
    
    
     <?php 
     
     include "classes/conn.php"; 

     ?> 
        




                <div class="col-xs-7 col-sm-5 col-md-8" style="background: white;  ">`
                    <div class="pull-center" style="font-size: 16px; text-align:center;"><b>
                        Republic of the Philippines<br>
                        Municipality of Consolacion<br>
                        Province of Cebu<br>
                        BARANGAY CABANGAHAN<br>
                        Tel. +63 917 585 9316.<br><br></b>
                    </div>
                  
                    <div class="pull-right" style="border: 2px ;">
                       
                    </div>

                       <?php   
                                    include("db.php"); 
                                    $sqlx = "select *   from   tbl_utilities where id_resident =".$_GET['id_resident']." ";
                                    $resultx = mysqli_query($conn, $sqlx); 
                                     
                                    while($rowx = mysqli_fetch_array($resultx)) 
                                    {
                                        $lname = $rowx["lname"];
                                        $fname = $rowx["fname"];
                                        $mi = $rowx["mi"];

                                        $address = $rowx["houseno"]; 
                                        

 
                                    }

                                    ?>


                    <div class="col-xs-12 col-md-12">
                    <div class="pull-left"><image align="left" src="./icons/cabangahanLOGO-removebg-preview.png" style="width:200%;height:80px;"/>  </div>

<div class="pull-right"><image align="right" src="./icons/cabangahanLOGO-removebg-preview.png" style="width:200%;height:80px;"/>  </div>
                        <p class="text-center" style="font-size: 20px; font-size:bold;">OFFICE OF THE BARANGAY CAPTAIN<br><br><b style="font-size: 25px;"><ins>REQUEST UTILITIES</ins></b></p> <br>
                        <p style="font-size: 18px;">TO WHOM IT MAY CONCERN:</p> <br>
                        <p style="text-indent:40px;text-align: justify;">This is to certify that <b><?= $resident['lname'];?>, <?= $resident['fname'];?> <?= $resident['mi'];?></b>,
                        <?= $resident['age'];?> Years Old,  and a bonafide resident of <?= $resident['brgy'];?> is allowed to borrow utilities from the barangay.</p> <br>
 
                       

                       


                        
                        
                        <br><br><br><br><br><br>
                    
                        <label style="font-size:18px;">____________</label>    <label style="font-size:18px;margin-left:4em;">Daisuke Hashimoto</label><br> 
                        <label style=" text-align: center;">Signature </label>     <label style=" text-align: center;margin-left:10em;">Punong Barangay</label>
                       
                        
                    </div>
                    
                </div>
                
                <div class="col-xs-offset-8 col-xs-5 col-md-offset-8 col-md-4 "  >
                
                </div>
 
                
                
            </div>
        </div>
    <button class="btn btn-primary noprint" id="printpagebutton" onclick="PrintElem('#clearance')">Print</button>
    </body>
    <?php
    
    ?>


    <script>
         function PrintElem(elem)
    {
        window.print();
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        //mywindow.document.write('<html><head><title>my div</title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        //mywindow.document.write('</head><body class="skin-black" >');
         var printButton = document.getElementById("printpagebutton");
        //Set the print button visibility to 'hidden' 
        printButton.style.visibility = 'hidden';
        mywindow.document.write(data);
        //mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();

        printButton.style.visibility = 'visible';
        mywindow.close();

        return true;
    }
    </script>
</html>