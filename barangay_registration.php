<?php 
     require('classes/resident.class.php');
    $residentbmis->create_barangay();
     //$data = $bms->get_userdata();

     
?>

<!DOCTYPE html> 
<html> 
    <head> 
        <title> Barangay Management System </title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/js/bootstrap-modalmanager.min.js" integrity="sha512-/HL24m2nmyI2+ccX+dSHphAHqLw60Oj5sK8jf59VWtFWZi9vx7jzoxbZmcBeeTeCUc7z1mTs3LfyXGuBU32t+w==" crossorigin="anonymous"></script>
        <!-- responsive tags for screen compatibility -->
        <meta name="viewport" content="width=device-width, initial-scale=1 shrink-to-fit=no">
        <!-- bootstrap css --> 
        <link href="./bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"> 
        <!-- fontawesome icons -->
        <script src="https://kit.fontawesome.com/67a9b7069e.js" crossorigin="anonymous"></script>
        <script src="https://f001.backblazeb2.com/file/buonzz-assets/jquery.ph-locations-v1.0.0.js"></script>
    </head>

    <style>
        
        .field-icon {
        margin-left: 74%;
        margin-top: -8%;
        position: absolute;
        z-index: 2;
        }

    </style>
    
    <body >

        <!-- eto yung navbar -->
        <nav class="navbar navbar-dark bg-primary sticky-top">
            <a class="navbar-brand" style="color: white;">Barangay Management System</a>
        </nav>

        <div class="container-fluid"  style="margin-top: 4em;">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-center">Barangay Registration Form</h1>
                    <br>
                </div>
            </div>

            <div class="row">
              <div class="col-7" style="margin: auto">
                <div class="card mbottom" style="margin-bottom: 3em;">
                        <form method="post" enctype='multipart/form-data' class="was-validated">
                        <div class="card-body" >
                            <b><i>*Barangay Admin or Officer Information</i></b>
                            <div class="card mbottom" style="margin-bottom: 3em;">
                                <div class="card-body" >
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label> Last Name: </label>
                                                <input type="text" class="form-control" name="lname"  placeholder="Enter Last Name" required>
                                                <div class="valid-feedback">Valid.</div>
                                                <div class="invalid-feedback">Please fill out this field.</div>
                                            </div>
                                        </div>
                                        
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="mtop" >First Name: </label>
                                                <input type="text" class="form-control" name="fname"  placeholder="Enter First Name" required>
                                                <div class="valid-feedback">Valid.</div>
                                                <div class="invalid-feedback">Please fill out this field.</div>
                                            </div>
                                        </div>

                                        <div class="col"> 
                                            <div class="form-group">
                                                <label class="mtop"> Middle Name: </label>
                                                <input type="text" class="form-control" name="mi" placeholder="Enter Middle Name" required>
                                                <div class="valid-feedback">Valid.</div>
                                                <div class="invalid-feedback">Please fill out this field.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="mtop">Contact Number:</label>
                                            <input type="tel" class="form-control" name="contact" maxlength="11" pattern="[0-9]{11}" placeholder="Enter Contact Number" required>
                                            <div class="valid-feedback">Valid.</div>
                                            <div class="invalid-feedback">Please fill out this field.</div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group">
                                            <label>Email: </label>
                                            <input type="email" class="form-control" name="email"  placeholder="Enter Email" required>
                                            <div class="valid-feedback">Valid.</div>
                                            <div class="invalid-feedback">Please fill out this field.</div>
                                        </div>
                                    </div>
                                    
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Password:</label>
                                            <input type="password" class="form-control" id="password-field" name="password" placeholder="Enter Password" required>
                                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                            <div class="valid-feedback">Valid.</div>
                                            <div class="invalid-feedback">Please fill out this field.</div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <b><i>*Barangay Information</i></b>
                            <div class="card mbottom" style="margin-bottom: 3em;">
                                <div class="card-body" >
                                    <div class="row">                              
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="mtop" >Province: </label>
                                                <input type="hidden" id="provvalue" name="province"/>
                                                <select class="form-control" id="my-provinces-dropdown"></select>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="form-group">
                                                <label class="mtop" >City: </label>
                                                <input type="hidden" id="cityvalue" name="city"/>
                                                <select class="form-control" id="my-cities-dropdown"></select>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="mtop" >Barangay: </label>
                                                <input type="hidden" id="brgyvalue" name="brgy"/>
                                                <select class="form-control" id="my-barangays-dropdown"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="mtop">Zip Code:</label>
                                            <input type="text" class="form-control" name="zipcode" placeholder="Zip Code:" required>
                                            <div class="valid-feedback">Valid.</div>
                                            <div class="invalid-feedback">Please fill out this field.</div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="mtop">Barangay Captain:</label>
                                            <input type="text" class="form-control" name="brgy_capt" placeholder="Barangay Captain Full name" required>
                                            <div class="valid-feedback">Valid.</div>
                                            <div class="invalid-feedback">Please fill out this field.</div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Account in Pending:</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h4>Please bring the following requirements:</h4>
                                    <br>
                                    <ul>
                                        <li>Valid ID of the appointed Barangay Admin / Staff</li>
                                        <li>Notarized Recommendation letter from the Barangay Captain</li>
                                        <li>Subscription Fee</li>
                                    </ul>  
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="submit" name="add_brgy">Submit</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <input type="hidden" class="form-control" name="role" value="resident">
                            <a style="width: 130px; margin-left:35%;" class="btn btn-danger" href="login.php"> Back to Login</a>
                            <!-- <button style="width: 130px;" class="btn btn-primary" type="submit" name="add_brgy"> Submis </button> -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                Submit
                            </button>
                        </div>
                        </form>
                    </div>  
                </div>
            </div>

            </div>
        </div>

        <!-- Footer -->
        <footer id="footer" class="bg-primary text-white d-flex-column text-center">

        </footer>

        <script>
            $('#my-provinces-dropdown').ph_locations({'location_type': 'provinces'});
            $('#my-provinces-dropdown').ph_locations( 'fetch_list', [{"province_code": "1339"}]);
            
            $('#my-provinces-dropdown').on('change', function() {
                var value = $(this).val();
                var textval = $("#my-provinces-dropdown option:selected").text();

                $(document).ready(function () {
                    $('#provvalue').val(textval);
                });

                $('#my-cities-dropdown').ph_locations({'location_type': 'cities'});
                $('#my-cities-dropdown').ph_locations( 'fetch_list', [{"province_code": value}]);
            });

            $('#my-cities-dropdown').on('change', function() {
                var value = $(this).val();
                var textval = $("#my-cities-dropdown option:selected").text();
                
                $(document).ready(function () {
                    $('#cityvalue').val(textval);
                });

                $('#my-barangays-dropdown').ph_locations({'location_type': 'barangays'});
                $('#my-barangays-dropdown').ph_locations( 'fetch_list', [{"city_code": value}]);
            });

            $('#my-barangays-dropdown').on('change', function() {
                var brgyval = $("#my-barangays-dropdown option:selected").text();
                $(document).ready(function () {
                    $('#brgyvalue').val(brgyval);
                });
            });
            

            $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
            input.attr("type", "text");
            } else {
            input.attr("type", "password");
            }
            });
            
        </script>

        <script src="./bootstrap/js/bootstrap.bundle.js" type="text/javascript"> </script>
    </body>
</html>

