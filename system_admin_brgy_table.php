<?php
	// require the database connection
	require 'classes/conn.php';
    //$bmis->update_status();
    //$residentbmis->update_status();
?>
	<table class="table table-hover text-center table-bordered table-responsive">

    <thead class="alert-info">
        <tr>
            <th> Province </th>
            <th> City </th>
            <th> Barangay </th>
            <th> Zip Code </th>
            <th> Barangay Captain </th>
            <th> Email </th>
            <th> Contact </th>
            <th> Actions</th>
        </tr>
    </thead>

    <tbody>     
        <?php
            
            $stmnt = $conn->prepare("SELECT tbl_barangay.brgy_id, barangay, city, province, zipcode, brgy_capt, email, role, contact, account_status, id_resident
            FROM tbl_barangay INNER JOIN tbl_resident ON tbl_barangay.brgy_id = tbl_resident.brgy_id
            WHERE account_status = 'active' and role = 'brgy_admin';");
            $stmnt->execute();
            
            while($view = $stmnt->fetch()){
        ?>
            <tr>
                <td width="250"> <?= $view['province'];?> </td> 
                <td width="250"> <?= $view['city'];?> </td>
                <td width="250"> <?= $view['barangay'];?> </td>
                <td width="200"> <?= $view['zipcode'];?> </td>
                <td width="250"> <?= $view['brgy_capt'];?> </td>
                <td> <?= $view['email'];?> </td>
                <td> <?= $view['contact'];?> </td>
                <td width="370"> 
                    <a class="btn btn-info" href="system_admin_brgy_view.php?brgy_id=<?php echo $view['brgy_id'];?>">View</a>   
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/js/bootstrap-modalmanager.min.js" integrity="sha512-/HL24m2nmyI2+ccX+dSHphAHqLw60Oj5sK8jf59VWtFWZi9vx7jzoxbZmcBeeTeCUc7z1mTs3LfyXGuBU32t+w==" crossorigin="anonymous"></script>
<!-- responsive tags for screen compatibility -->
<meta name="viewport" content="width=device-width, initial-scale=1 shrink-to-fit=no">
<!-- custom css --> 
<link href="../BarangaySystem/customcss/regiformstyle.css" rel="stylesheet" type="text/css">
<!-- bootstrap css --> 
<link href="./bootstrap//css/bootstrap.css" rel="stylesheet" type="text/css"> 
<!-- fontawesome icons -->
<script src="https://kit.fontawesome.com/67a9b7069e.js" crossorigin="anonymous"></script>
<script src="./bootstrap//js/bootstrap.bundle.js" type="text/javascript"> </script>

<?php
$con = null;
?>