<?php
?>
<!DOCTYPE html>
<html lang="en">
    

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
        <meta charset="utf-8">
        <!-- Responsive Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Compatibility -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Meta Description -->
        <meta name="description" content="...">

        <!-- Page title -->
        <title>Barangay Management System</title>

        
        <!-- <link rel="shortcut icon" href="icons/favicon.ico"> -->

        <link href="https://fonts.googleapis.com/css?family=Saira" rel="stylesheet">
    



        <!-- Font Awesome Stylesheet -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <!-- Bootstrap Stylesheet -->
        <link rel='stylesheet' href='css/bootstrap.min.css'/>
        <!-- Lightbox Stylesheet -->
        <link rel='stylesheet' href='css/jquery.fancybox.min.css'/>
        <!-- Animate.CSS Stylesheet -->
        <link rel='stylesheet' href='css/animate.css'/>
        <!-- Custom Stylesheet -->
        <link rel='stylesheet' href='css/style.default.css'/>
        <link rel='stylesheet' href='css/button.css'/>


    </head>
    <body>



        <!-- BLOCK HOME -->

                    <div id="home" class="content_diver home  " title="home" onmouseover="this.title='';" >
                        <div id="top_slider" class="slider_page  top_slider" align="center">
                            <div style="  z-index :99;position: absolute;   overflow: hidden; width:100%; top:0px; bottom:0px;"  >
                                <canvas id="demo-canvas"  ></canvas>
                            </div>
                            <div class="container d-flex align-items-center">

                <div class="heading" style="margin: auto; position: relative; top: 10em">
                    <h1 class="wow fadeInUp" data-wow-delay="0.3s">Welcome to<br> Barangay Management System<span style="opacity: 0.8;" class="ellipsis"></span></h1>
                    <br><br>
                    <a class="button" href="login.php"> Get Started</a>
                </div>
               </div>
             </div>
            </div>
                 
               

        <!--=============== About Section ===============-->
        <section class="about" id="about">
            <div class="container text-center">
                <h2 class="">What is Barangay Management System?</h2>
                <p class="lead">
                    A Barangay Management System is a computer-based system designed to manage and automate the operations of a barangay. 
                    It is a tool that allows local government units to efficiently and effectively manage their services and programs at the barangay level.
                    The system typically includes modules for managing resident information, barangay finances, permits and licenses, blotter, scholarship, utilities.
                    The Barangay Management System can help streamline administrative processes, improve service delivery, and enhance transparency and accountability in the management of barangay affairs.
                    It is a valuable tool for local governments in improving their services to their constituents.

                </p>
                
            </div>
        </section>

        <section class="about" id="about">
            <div class="container text-center">
                <h2 class="">How to use?</h2>
                <p class="lead">
                    May use <b>Laptop</b> and <b>Desktop Computer</b> in accessing this system.
                </p>
                
            </div>
        </section>

        <!--=============== Portfolio Section ===============-->
        <section class="portfolio gray-bg has-gradient-both" id="portfolio">
            <div class="container">
            
                <!-- Gallery Navigation -->
                <div class="gallery text-center">
        

                    <!-- Gallery Items -->
                    <div id="gallery" class="row">
                        

                      




          
                    </div>
                </div>
            </div>
        </section>
        <!--=============== End Portfolio Section ===============-->




        <!--=============== Scroll To Top Button ===============-->
        <div id="scrollTop" class="text-center ellipsis">
            <i class="fa fa-angle-up"></i>
        </div>
        <!--=============== End Scroll To Top Button ===============-->


        <!--=============== Preloader ===============-->
        <div class="preloader">
            <div class="preloader-inner d-flex align-items-center justify-content-center">

                <div class="circle-big">
                    <div class="circle-small"></div>
                </div>

            </div>
        </div>
        <!--=============== End Preloader ===============-->




        <!-- Jquery CDN -->
        <script src="js/jquery.min.js"></script>
        <!-- Tether CDN [required for Bootstrap 4 tooltip]-->
        <script src="js/tether.min.js"></script>
        <!-- Bootstrap -->
        <script src='js/bootstrap.min.js'></script>
        <!-- Mixitup -->
        <script src='js/mixitup.min.js'></script>
        <!-- FancyBox -->
        <script src='js/jquery.fancybox.min.js'></script>
        <!-- Counter Up -->
        <script src="js/waypoints.min.js"></script>
        <script src='js/jquery.counterup.min.js'></script>
        <!-- Smooth Scroll -->
        <script src='js/easyscroll.min.js'></script>
        <!-- WOW.Js -->
        <script src='js/wow.min.js'></script>
        <!-- jQuery Validator -->
        <script src='js/jquery.validate.min.js'></script>
        <!-- Custom Script -->
        <script src='js/script.js'></script>

        
    <script src="js/TweenLite.min.js"></script>
    <script src="js/EasePack.min.js"></script>
    <script src="js/demo-1.js"></script>

    </body>


</html>
