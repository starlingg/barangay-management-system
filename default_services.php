<div class="row">
<div class="col">
    <div class="custom-control custom-switch">
        <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch1" value="Barangay Clearance">
        <label class="custom-control-label" for="customSwitch1">Barangay Clearance</label>
    </div>
</div>
<div class="col">
    <div class="custom-control custom-switch">
        <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch2" value="Barangay Permit">
        <label class="custom-control-label" for="customSwitch2">Barangay Permit</label>
    </div>
</div>
<div class="col">
    <div class="custom-control custom-switch">
        <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch3" value="Certificate of Indigency">
        <label class="custom-control-label" for="customSwitch3">Certificate of Indigency</label>
    </div>
</div>
</div>
<br>
<div class="row">
<div class="col">
    <div class="custom-control custom-switch">
        <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch4" value="Certificate of Recidency">
        <label class="custom-control-label" for="customSwitch4">Certificate of Recidency</label>
    </div>
</div>
<div class="col">
    <div class="custom-control custom-switch">
        <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch5" value="Scholarship">
        <label class="custom-control-label" for="customSwitch5">Scholarship</label>
    </div>
</div>
<div class="col">
    <div class="custom-control custom-switch">
        <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch6" value="Utilities">
        <label class="custom-control-label" for="customSwitch6">Utilities</label>
    </div>
</div>
</div>
<br>
<div class="row">
<div class="col">
    <div class="custom-control custom-switch">
        <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch7" value="Blotter">
        <label class="custom-control-label" for="customSwitch7">Blotter</label>
    </div>
</div>
</div>