<?php 

    require_once('main.class.php');
    

    class ResidentClass extends BMISClass {
        //------------------------------------ RESIDENT CRUD FUNCTIONS ----------------------------------------

        public function create_resident() {
            if(isset($_POST['add_resident'])) {
                $email = $_POST['email'];
                $password = ($_POST['password']);
                $lname = $_POST['lname'];
                $fname = $_POST['fname'];
                $mi = $_POST['mi'];
                $age = $_POST['age'];
                $sex = $_POST['sex'];
                $status = $_POST['status'];
                $houseno = $_POST['houseno'];
                $street = $_POST['street'];
                $brgy = $_POST['brgy'];
                $municipal = $_POST['municipal'];
                $contact = $_POST['contact'];
                $bdate = $_POST['bdate'];
                $bplace = $_POST['bplace'];
                $nationality = $_POST['nationality'];
                $voter = $_POST['voter'];
                $familyrole = $_POST['family_role'];
                $role = $_POST['role'];
                $addedby = $_POST['addedby'];
                $brgy_id = $_POST['brgy_id'];

                $min_age = 18;
                $max_age = 150;

                if ($this->check_resident($email) == 0 ) {

                    if(!in_array( $age, range( $min_age, $max_age) ) ){
                        $message1 = "Sorry, you are still underaged to register an account";
                        echo "<script type='text/javascript'>alert('$message1');</script>";
                        return(0);
                    }
    
                    else {

                        $connection = $this->openConn();
                        $stmt = $connection->prepare("INSERT INTO tbl_resident ( `email`,`password`,`lname`,`fname`,
                        `mi`, `age`, `sex`, `status`, `houseno`, `street`, `brgy`, `municipal`, `contact`, `bdate`, 
                        `bplace`, `nationality`,`voter` ,`family_role`,
                        `role`, `addedby`, `brgy_id`, `account_status`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?)");
    
                        $stmt->Execute([ $email, $password, $lname, $fname, $mi, $age, $sex, $status, 
                        $houseno, $street, $brgy, $municipal, $contact, $bdate, $bplace, $nationality, $voter, $familyrole, $role, $addedby, $brgy_id, 'active']);

                        $message2 = "Account added, you can now continue logging in";
                        echo "<script type='text/javascript'>alert('$message2');</script>";

                        header("Refresh:0");
                    }
                }

                else {
                    echo "<script type='text/javascript'>alert('Email Account already exists');</script>";
                }
            }
        }

        public function view_resident(){
            $connection = $this->openConn();
            $stmt = $connection->prepare("SELECT * from tbl_resident");
            $stmt->execute();
            $view = $stmt->fetchAll();
            return $view;
        }

        public function create_barangay() {
            if(isset($_POST['add_brgy'])) { 
                $lname = $_POST['lname'];
                $fname = $_POST['fname'];
                $mi = $_POST['mi'];
                $contact = $_POST['contact'];
                $email = $_POST['email'];
                $password = $_POST['password'];
                $province = $_POST['province'];
                $city = $_POST['city'];
                $brgy = $_POST['brgy'];
                $zipcode = $_POST['zipcode'];
                $brgy_capt = $_POST['brgy_capt'];
                $connection = $this->openConn();
                
                //INSERT BARANGAY INFORMATION
                $insert_brgy = $connection->prepare("INSERT INTO `tbl_barangay`(`barangay`, `city`, `province`, `zipcode`, `brgy_capt`)
                    VALUES (?, ?, ?, ?, ?)");
                $insert_brgy->Execute([$brgy, $city, $province, $zipcode, $brgy_capt]);

                //GET ID OF THE LATEST INSERTED BARANGAY
                $brgy_id = $connection->prepare("SELECT LAST_INSERT_ID()");
                $brgy_id->execute();
                $result = $brgy_id->fetchColumn();
            
                //INSERT BRGY ADMIN INFORMATION
                $insert_brgy_admin = $connection->prepare("INSERT INTO `tbl_resident`(`email`, `password`, `lname`, `fname`, `mi`, `brgy`, `municipal`, `contact`, `role`, `brgy_id`, `account_status`) 
                VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                $insert_brgy_admin->Execute([$email, $password, $lname, $fname, $mi, $brgy, $city, $contact, 'brgy_admin', $result, 'pending']);

                
                echo "<script type='text/javascript'>
                    alert('SUCCESSFULL REGISTERED!!');
                    window.location.href='login.php';
                </script>";
            }
        }

        public function update_status() {
            $id_resident = $_POST['id_resident'];

            if(isset($_POST['approveBtn'])) {
                $connection = $this->openConn();
                $stmt = $connection->prepare("UPDATE tbl_resident SET account_status= ? WHERE id_resident = ?");
                $stmt->execute(['active', $id_resident]);
                
                echo "<script type='text/javascript'>
                    alert('Barangay Account has been successfully Activated!');
                    window.location.href='system_admin_barangays.php';
                </script>";
                header("refresh: 0");
            }
            else if(isset($_POST['disapproveBtn'])) {
                $connection = $this->openConn();
                $stmt = $connection->prepare("UPDATE tbl_resident SET `account_status`= ? WHERE `id_resident` = ?");
                $message2 = "Barangay Account Request has been disapproved!";
                echo "<script type='text/javascript'>
                    alert('Barangay Account Request has been disapproved!');
                    window.location.href='system_admin_barangays.php';
                </script>";
            }
        }

        public function get_brgy_info($brgy_id){

            $brgy_id = $_GET['brgy_id'];
            
            $connection = $this->openConn();
            $stmt = $connection->prepare("SELECT * FROM tbl_barangay where brgy_id = ?");
            $stmt->execute([$brgy_id]);
            $barangay = $stmt->fetch();
            $total = $stmt->rowCount();
    
            if($total > 0 )  {
                return $barangay;
            }
            else{
                return false;
            }
        }

        
        public function get_brgy_srvs($brgy_id){

            //$brgy_id = $_GET['brgy_id'];

            $connection = $this->openConn();
            $stmt = $connection->prepare("SELECT * FROM tbl_brgy_services where brgy_id = ?");
            $stmt->execute([$brgy_id]);
            $brgy_srvs = $stmt->fetch();
            $total = $stmt->rowCount();
    
            if($total > 0 )  {
                return $brgy_srvs;
            }
            else{
                return false;
            }
        }

        public function get_brgy_email($brgy_id){

            $brgy_id = $_GET['brgy_id'];
            
            $connection = $this->openConn();
            $stmt = $connection->prepare("SELECT * FROM tbl_resident where brgy_id = ? and role = ?");
            $stmt->execute([$brgy_id, 'brgy_admin']);
            $barangay = $stmt->fetch();
            $total = $stmt->rowCount();
    
            if($total > 0 )  {
                return $barangay;
            }
            else{
                return false;
            }
        }

        public function update_brgy_info() {
            if(isset($_POST['update_brgy_info'])) {
                $province = $_POST['province'];
                $city = $_POST['city'];
                $barangay = $_POST['barangay'];
                $zipcode = $_POST['zipcode'];
                $brgy_capt = $_POST['brgy_capt'];
                $account = $_POST['account'];
                $services = $_POST['services'];
                $brgy_id = $_POST['brgy_id'];
                $services_serialized = serialize($services);

                $connection = $this->openConn();
                //update resident table
                $resident_tbl = $connection->prepare("UPDATE tbl_resident SET `email` = ? WHERE `brgy_id` = ? AND `role` = ? AND `account_status` = ?");
                $resident_tbl->execute([$account, $brgy_id, 'brgy_admin', 'active']);

                //update brgy table
                $brgy_table  = $connection->prepare("UPDATE tbl_barangay SET `barangay` = ?, `city` = ?, `province` = ?, `zipcode` = ?, `brgy_capt` = ?  
                    WHERE `brgy_id` = ?");
                $brgy_table->execute([$barangay, $city, $province, $zipcode, $brgy_capt, $brgy_id]);

                //check if has services
                $stmt = $connection->prepare("SELECT * FROM tbl_brgy_services WHERE brgy_id = ?");
                $stmt->execute([$brgy_id]);
                $total = $stmt->rowCount();

                if($total == 0) {
                    // insert brgy_services
                    $insert_brgy_srv = $connection->prepare("INSERT INTO `tbl_brgy_services`(`brgy_id`, `brgy_services`) 
                    VALUES ( ?, ?)");
                    $insert_brgy_srv->Execute([$brgy_id, $services_serialized]);    
                } else {
                    //update brgy_services
                    $update_brgy_srv = $connection->prepare("UPDATE tbl_brgy_services SET `brgy_services` = ? WHERE `brgy_id` = ?");
                    $update_brgy_srv->execute([$services_serialized, $brgy_id]);  
                }

                echo "<script type='text/javascript'>alert('Successfully Updated!');</script>";
                header("refresh: 0");
            }
        }

        public function update_resident() {
            if (isset($_POST['update_resident'])) {
                $id_resident = $_GET['id_resident'];
                $email = $_POST['email'];
                $password = ($_POST['password']);
                $lname = $_POST['lname'];
                $fname = $_POST['fname'];
                $mi = $_POST['mi'];
                $age = $_POST['age'];
                $sex = $_POST['sex'];
                $status = $_POST['status'];
                $houseno = $_POST['houseno'];
                $street = $_POST['street'];
                $brgy = $_POST['brgy'];
                $municipal = $_POST['municipal'];
                $contact = $_POST['contact'];
                $bdate = $_POST['bdate'];
                $bplace = $_POST['bplace'];
                $nationality = $_POST['nationality'];
                $voter = $_POST['voter'];
                $familyrole = $_POST['family_role'];
                $role = $_POST['role'];
                $addedby = $_POST['addedby'];

                $connection = $this->openConn();
                $stmt = $connection->prepare("UPDATE tbl_resident SET `password` =?, `lname` =?, 
                `fname` = ?, `mi` =?, `age` =?, `sex` =?, `status` =?, `email` =?, `houseno` =?, `street` =?,
                `brgy` =?, `municipal` =?, `contact` =?,
                `bdate` =?, `bplace` =?, `nationality` =?, `voter` =?, `family_role` =?, `role` =?, `addedby` =? WHERE `id_resident` = ?");
                $stmt->execute([$password, $lname, $fname, $mi, $age, $sex, $status,$email, $houseno, 
                $street, $brgy, $municipal,
                $contact, $bdate, $bplace, $nationality, $voter, $familyrole, $role, $addedby, $id_resident]);
                    
                $message2 = "Resident Data Updated";
                echo "<script type='text/javascript'>alert('$message2');</script>";
                header("refresh: 0");
            }
        }

        public function delete_resident(){
            $id_resident = $_POST['id_resident'];

            if(isset($_POST['delete_resident'])) {
                $connection = $this->openConn();
                $stmt = $connection->prepare("DELETE FROM tbl_resident where id_resident = ?");
                $stmt->execute([$id_resident]);

                $message2 = "Resident Data Deleted";
                
                echo "<script type='text/javascript'>alert('$message2');</script>";
                header("Refresh:0");
            }
        }

    //-------------------------------- EXTRA FUNCTIONS FOR RESIDENT CLASS ---------------------------------

    


    public function get_single_resident($id_resident){

        $id_resident = $_GET['id_resident'];
        
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT * FROM tbl_resident where id_resident = ?");
        $stmt->execute([$id_resident]);
        $resident = $stmt->fetch();
        $total = $stmt->rowCount();

        if($total > 0 )  {
            return $resident;
        }
        else{
            return false;
        }
    }
   
    public function check_resident($email) {

        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT * FROM tbl_resident WHERE email = ?");
        $stmt->Execute([$email]);
        $total = $stmt->rowCount(); 

        return $total;
    }

    public function count_resident() {
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT COUNT(*) from tbl_resident");
        $stmt->execute();
        $rescount = $stmt->fetchColumn();
        return $rescount;
    }

    public function check_household($lname, $mi) {
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT * FROM tbl_resident WHERE lname = ? AND mi = ?");
        $stmt->Execute([$lname, $mi]);
        $total = $stmt->rowCount(); 
        return $total;
    }

    public function view_household_list() {
        $lname = $_POST['lname'];
        $mi = $_POST['mi'];

        if(isset($_POST['search_household'])) {
            $connection = $this->openConn();
            $stmt1 = $connection->prepare("SELECT * FROM `tbl_resident` WHERE `lname` LIKE '%$lname%' and  `mi` LIKE '%$mi%'");
            $stmt1->execute();
        }
    }

    public function count_male_resident() {
        $connection = $this->openConn();

        $stmt = $connection->prepare("SELECT COUNT(*) from tbl_resident where sex = 'male' ");
        $stmt->execute();
        $rescount = $stmt->fetchColumn();

        return $rescount;
    }

    public function count_female_resident() {
        $connection = $this->openConn();

        $stmt = $connection->prepare("SELECT COUNT(*) from tbl_resident where sex = 'female'");
        $stmt->execute();
        $rescount = $stmt->fetchColumn();

        return $rescount;
    }

    public function count_head_resident() {
        $connection = $this->openConn();

        $stmt = $connection->prepare("SELECT COUNT(*) from tbl_resident where family_role = 'Yes'");
        $stmt->execute();
        $rescount = $stmt->fetchColumn();

        return $rescount;
    }

    public function count_member_resident() {
        $connection = $this->openConn();

        $stmt = $connection->prepare("SELECT COUNT(*) from tbl_resident where family_role = 'Family Member'");
        $stmt->execute();
        $rescount = $stmt->fetchColumn();

        return $rescount;
    }

    public function profile_update() {
        $id_resident = $_GET['id_resident'];
        $age = $_POST['age'];
        $status = $_POST['status'];
        $address = $_POST['address'];
        $contact = $_POST['contact'];

        if (isset($_POST['profile_update'])) {
           
            $connection = $this->openConn();
            $stmt = $connection->prepare("UPDATE tbl_resident SET  `age` = ?,  `status` = ?, 
            `address` = ?, `contact` = ? WHERE id_resident = ?");
            $stmt->execute([ $age, $status, $address,
            $contact, $id_resident]);
               
            $message2 = "Resident Profile Updated";
                
            echo "<script type='text/javascript'>alert('$message2');</script>";
            header("Refresh:0");

        }

    }
    

    //------------------------------------- RESIDENT FILTERING QUERIES --------------------------------------

    public function view_resident_minor(){
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT * FROM tbl_resident WHERE `age` <= 17");
        $stmt->execute();
        $view = $stmt->fetchAll();
        return $view;
    }

    public function view_resident_adult(){
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT * FROM tbl_resident WHERE `age` >= 18 AND `age` <= 59");
        $stmt->execute();
        $view = $stmt->fetchAll();
        return $view;
    }

    public function view_resident_senior(){
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT * FROM tbl_resident WHERE `age` >= 60");
        $stmt->execute();
        $view = $stmt->fetchAll();
        return $view;
    }

    public function count_resident_senior() {
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT COUNT(*) FROM tbl_resident WHERE `age` >= 60");
        $stmt->execute();
        $rescount = $stmt->fetchColumn();

        return $rescount;
    }





    //-------------------------------------- EXTRA FUNCTIONS ------------------------------------------------

    public function resident_changepass() {
        $id_resident = $_GET['id_resident'];
        $oldpassword = ($_POST['oldpassword']);
        $oldpasswordverify = ($_POST['oldpasswordverify']);
        $newpassword = ($_POST['newpassword']);
        $checkpassword = $_POST['checkpassword'];

        if(isset($_POST['resident_changepass'])) {

            $connection = $this->openConn();
            $stmt = $connection->prepare("SELECT `password` FROM tbl_resident WHERE id_resident = ?");
            $stmt->execute([$id_resident]);
            $result = $stmt->fetch();

            if($result == 0) {
                
                echo "Old Password is Incorrect";
            }

            elseif ($oldpassword != $oldpasswordverify) {
                echo "Old Password is Incorrect";
            }

            elseif ($newpassword != $checkpassword){
                echo "New Password and Verification Password does not Match";
            }

            else {
                $connection = $this->openConn();
                $stmt = $connection->prepare("UPDATE tbl_resident SET password = '$newpassword'  WHERE id_resident = '$id_resident'");
                $stmt->execute([$newpassword, $id_resident]);
                
                $message2 = "Password Updated";
                echo "<script type='text/javascript'>alert('$message2');</script>";
                header("refresh: 0");
            }


        }
    }





    //========================================== SCOPE CHANGED FUNCTIONS ===========================================

    public function view_resident_household(){
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT * from tbl_resident WHERE `family_role` = 'Yes'");
        $stmt->execute();
        $view = $stmt->fetchAll();
        return $view;
    }

    public function view_resident_voters(){
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT * from tbl_resident WHERE `voter` = 'Yes'");
        $stmt->execute();
        $view = $stmt->fetchAll();
        return $view;
    }

    public function view_resident_male(){
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT * from tbl_resident WHERE `sex` = 'Male'");
        $stmt->execute();
        $view = $stmt->fetchAll();
        return $view;
    }

    public function view_resident_female(){
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT * from tbl_resident WHERE `sex` = 'Female'");
        $stmt->execute();
        $view = $stmt->fetchAll();
        return $view;
    }

    public function count_voters() {
        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT COUNT(*) from tbl_resident where `voter` = 'Yes' ");
        $stmt->execute();
        $rescount = $stmt->fetchColumn();

        return $rescount;
    }


    
    

    public function search_admn_voter() {
        
        $search = $_GET['search'];

        $connection = $this->openConn();
        $stmt = $connection->prepare("SELECT * from tbl_resident WHERE `fname` = '$search'");
        $stmt->execute();
        $view = $stmt->fetchAll();
        return $view;

            


            
        
        

    }








    }

    $residentbmis = new ResidentClass();
?>