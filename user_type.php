<?php 
    error_reporting(E_ALL ^ E_WARNING);
    
    if(!isset($_SESSION)) {
        $showdate = date("Y-m-d");
        date_default_timezone_set('Asia/Manila');
        $showtime = date("h:i:a");
        $_SESSION['storedate'] = $showdate;
        $_SESSION['storetime'] = $showdate;
        session_start();
    }



    //include('autoloader.php');    
    require('classes/main.class.php');
    $bmis->login();

   
?>

<!DOCTYPE html> 
<html> 
    <head> 
        <title> Barangay Management System </title>
        <!-- responsive tags for screen compatibility -->
        <meta name="viewport" content="width=device-width, initial-scale=1 shrink-to-fit=no">
        <!-- custom css --> 
        <link href="../BarangaySystem/css/index.css" rel="stylesheet" type="text/css">
        <!-- bootstrap css -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"> 
        <!-- fontawesome icons --> 
        <script src="https://kit.fontawesome.com/67a9b7069e.js" crossorigin="anonymous"></script>
        <!-- fontawesome icons --> 
        <script src="../BarangaySystem/customjs/main.js" type="text/javascript"></script>

        <style> 
            body {
                background-color: #00405B !important;
            }
            .input-container {
            display: -ms-flexbox; /* IE10 */
            display: flex;
            width: 100%;
            margin-bottom: 10px;
            }

            .icon {
            padding: 15px;
            background: dodgerblue;
            color: white;
            min-width: 50px;
            text-align: center;
            }

            .input-field {
            width: 100%;
            padding: 10px;
            outline: none;
            }

            .input-field:focus {
            border: 2px solid dodgerblue;
            }

            /* Set a style for the submit button */
            .btn {
            color: white;
            padding: 10px 15px;
            border: none;
            cursor: pointer;
            width: 100%;
            opacity: 0.9;
            }

            .btn:hover {
            opacity: 1;
            }
        </style>

    </head>



    <body>


        <!-- This is the heading and card section --> 
        <section class="main-section"> 
        <div class="container-fluid"> 
            <div class="row">
                <div class="col-sm"></div>
                    <div class="col-sm main-heading text-center text-white m-5" > 
                        <h1> Barangay Management System </h1> 
                    </div>
                <div class="col-sm"></div>
            </div>
        <div class="row" style="margin-top: 2em">
            <div class="col-lg-3 mb-4"></div>
            <div class="col-lg-3 mb-4">
                <div class="card">
                    <img class="card-img-top" src="" alt="">
                    <div class="card-body">
                        <h5 class="card-title">Barangay</h5>
                        <p class="card-text">
                           Barangay Staff or Officials can register their barangay to take full access on services we offer.
                        </p>
                        <br>
                        <hr />
                        <a href="barangay_registration.php" class="btn btn-primary btn-sm">
                            Register
                        </a>
                        <br /> <br />
                        <a href="login.php" class="btn btn-secondary btn-sm">
                            Cancel</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 mb-4">
                <div class="card">
                    <img class="card-img-top" src="" alt="">
  
                    <div class="card-body">
                        <h5 class="card-title">Barangay Resident</h5>
                        <p class="card-text">
                            Barangay resident are the people living in a certain barangay. Blah blah blah
                        </p>
                          
                        <br>
                        <hr />
                        <a href="resident_registration.php" class="btn btn-primary btn-sm">
                            Register
                        </a>
                        <br /> <br />
                        <button href="login.php" class="btn btn-secondary btn-sm">
                            Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

        </section>

        <!-- Footer -->

        <footer id="footer" class="bg-primary text-white d-flex-column text-center">

            <!--Copyright-->



        </footer>

        <script>
            function myFunction() {
                var x = document.getElementById("myInput");
                    if (x.type === "password") {
                        x.type = "text";
                    } else {
                        x.type = "password";
                }
            }

            function trying() {
                window.location.href = "resident_registration.php";
            }
        </script>

    </body>
</html>