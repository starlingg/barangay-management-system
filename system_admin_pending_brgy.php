<?php
	// require the database connection
	require 'classes/conn.php';
    //$bmis->update_status();
    $residentbmis->update_status();
?>
	<table class="table table-hover text-center table-bordered table-responsive">

    <thead class="alert-info">
        <tr>
            <th> Province </th>
            <th> City </th>
            <th> Barangay </th>
            <th> Zip Code </th>
            <th> Barangay Captain </th>
            <th> Email </th>
            <th> Contact </th>
            <th> Account Status </th>
            <th> Actions</th>
        </tr>
    </thead>

    <tbody>     
        <?php
            
            $stmnt = $conn->prepare("SELECT barangay, city, province, zipcode, brgy_capt, email, role, contact, account_status, id_resident
            FROM tbl_barangay INNER JOIN tbl_resident ON tbl_barangay.brgy_id = tbl_resident.brgy_id
            WHERE account_status = 'pending' and role = 'brgy_admin';");
            $stmnt->execute();
            
            while($view = $stmnt->fetch()){
        ?>
            <tr>
                <td> <?= $view['province'];?> </td> 
                <td> <?= $view['city'];?> </td>
                <td> <?= $view['barangay'];?> </td>
                <td> <?= $view['zipcode'];?> </td>
                <td> <?= $view['brgy_capt'];?> </td>
                <td> <?= $view['email'];?> </td>
                <td> <?= $view['contact'];?> </td>
                <td> <?= $view['account_status'];?>
                <td width="370"> 
                    <form action="" method="post">
                        <input type="hidden" name="id_resident" value="<?= $view['id_resident'];?>">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#approvedModal">
                            Approve
                        </button>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#disaaprovedModal">
                            Disapprove
                        </button>

                        <div class="modal fade" id="approvedModal" tabindex="-1" role="dialog" aria-labelledby="approvedModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Account Approval Confirmation</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                   You are about to approve this request. Please make sure that the following requirements has been successfully submitted:
                                    -dsfad
                                    -safdsa
                                    -dsaf
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="submit" name="approveBtn" class="btn btn-primary">Confirm</button>
                                </div>
                                </div>
                            </div>
                        </div>

                        <!-- Dissaprove modal here -->
                        <div class="modal fade" id="disaaprovedModal" tabindex="-1" role="dialog" aria-labelledby="disaaprovedModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Account Disapproval</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    You are about to disapproved this request. Are you sure you want to continue?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="submit" name="disapproveBtn" class="btn btn-primary">Confirm</button>
                                </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/js/bootstrap-modalmanager.min.js" integrity="sha512-/HL24m2nmyI2+ccX+dSHphAHqLw60Oj5sK8jf59VWtFWZi9vx7jzoxbZmcBeeTeCUc7z1mTs3LfyXGuBU32t+w==" crossorigin="anonymous"></script>
<!-- responsive tags for screen compatibility -->
<meta name="viewport" content="width=device-width, initial-scale=1 shrink-to-fit=no">
<!-- custom css --> 
<link href="../BarangaySystem/customcss/regiformstyle.css" rel="stylesheet" type="text/css">
<!-- bootstrap css --> 
<link href="./bootstrap//css/bootstrap.css" rel="stylesheet" type="text/css"> 
<!-- fontawesome icons -->
<script src="https://kit.fontawesome.com/67a9b7069e.js" crossorigin="anonymous"></script>
<script src="./bootstrap//js/bootstrap.bundle.js" type="text/javascript"> </script>

<?php
$con = null;
?>