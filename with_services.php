<div class="row">
<div class="col">
<?php 
    $arr = unserialize($brgy_srvs['brgy_services']);
    $search = 'Barangay Clearance';
    $isPresent = array_search($search,$arr,true);
    
    echo '<div class="custom-control custom-switch">
        <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch1" value="Barangay Clearance" '.($isPresent != '' ? "checked" : "").'>
        <label class="custom-control-label" for="customSwitch1">Barangay Clearance</label>
    </div>';
    
?>
</div>
<div class="col">
<?php 
    $arr = unserialize($brgy_srvs['brgy_services']);
    $search = 'Barangay Permit';
    $isPresent = array_search($search,$arr,true);
    
    echo '<div class="custom-control custom-switch">
        <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch2" value="Barangay Permit" '.($isPresent != '' ? "checked" : "").'>
        <label class="custom-control-label" for="customSwitch2">Barangay Permit</label>
    </div>';
    
?>
</div>
<div class="col">
    <?php 
        $arr = unserialize($brgy_srvs['brgy_services']);
        $search = 'Certificate of Indigency';
        $isPresent = array_search($search,$arr,true);
        
        echo '<div class="custom-control custom-switch">
            <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch3" value="Certificate of Indigency" '.($isPresent != '' ? "checked" : "").'>
            <label class="custom-control-label" for="customSwitch3">Certificate of Indigency</label>
        </div>';
    ?>
</div>
</div>
<br>
<div class="row">
<div class="col">
    <?php 
        $arr = unserialize($brgy_srvs['brgy_services']);
        $search = 'Certificate of Recidency';
        $isPresent = array_search($search,$arr,true);
        
        echo '<div class="custom-control custom-switch">
            <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch4" value="Certificate of Recidency" '.($isPresent != '' ? "checked" : "").'>
            <label class="custom-control-label" for="customSwitch4">Certificate of Recidency</label>
        </div>';
    ?>
</div>
<div class="col">
    <?php 
        $arr = unserialize($brgy_srvs['brgy_services']);
        $search = 'Scholarship';
        $isPresent = array_search($search,$arr,true);
        
        echo '<div class="custom-control custom-switch">
            <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch5" value="Scholarship" '.($isPresent != '' ? "checked" : "").'>
            <label class="custom-control-label" for="customSwitch5">Scholarship</label>
        </div>';
    ?>
</div>
<div class="col">
    <?php 
        $arr = unserialize($brgy_srvs['brgy_services']);
        $search = 'Utilities';
        $isPresent = array_search($search,$arr,true);
        
        echo '<div class="custom-control custom-switch">
            <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch6" value="Utilities" '.($isPresent != '' ? "checked" : "").'>
            <label class="custom-control-label" for="customSwitch6">Utilities</label>
        </div>';
    ?>
</div>
</div>
<br>
<div class="row">
<div class="col">
    <?php 
        $arr = unserialize($brgy_srvs['brgy_services']);
        $search = 'Blotter';
        $isPresent = array_search($search,$arr,true);
        
        echo '<div class="custom-control custom-switch">
            <input type="checkbox" name="services[]" class="custom-control-input" id="customSwitch7" value="Blotter" '.($isPresent != '' ? "checked" : "").'>
            <label class="custom-control-label" for="customSwitch7">Blotter</label>
        </div>';
    ?>
</div>
</div>